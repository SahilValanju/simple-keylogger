package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"time"
)

func main() {
	keyLoggerFile, err := os.Create("keylogger.txt")
	if err != nil {
		log.Fatalf("Error creating file: %v", err)
	}
	defer keyLoggerFile.Close()

	reader := bufio.NewReader(os.Stdin)

	for {
		// Read a character from stdin and log it to the file
		char, _, err := reader.ReadRune()
		if err != nil {
			log.Println("Error reading from input:", err)
			continue
		}

		timestamp := time.Now().Format(time.UnixDate)
		keyLoggerFile.WriteString(fmt.Sprintf("%s - %c\n", timestamp, char))
	}
}
